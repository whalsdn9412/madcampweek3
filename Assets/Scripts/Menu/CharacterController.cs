﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class CharacterController : NetworkBehaviour
{
    public GameObject Player;
    public Camera PlayerCamera;
    public Vector3 DifferPlayerAndCamera;
    public float speed;
    Animator ani;
    Rigidbody rbody;
    // Start is called before the first frame update
    private void Start()
    {
        if (!isLocalPlayer)
        {
            PlayerCamera.enabled = false;
            return;
        }
        rbody = Player.GetComponent<Rigidbody>();
        ani = GetComponent<Animator>();
    }


    private void Awake()
    {
        DifferPlayerAndCamera = Player.transform.position - PlayerCamera.transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        if (!isLocalPlayer)
        {
            PlayerCamera.enabled = false;
            return;
        }

        var x = Input.GetAxis("Horizontal") * Time.deltaTime * 50.0f * speed;
        var z = Input.GetAxis("Vertical") * Time.deltaTime * 1.0f * speed;
        Player.transform.Translate(0, 0, z);
        Player.transform.Rotate(0, x, 0);
        if (x == 0 && z == 0) ani.SetBool("RunChk", false);
        else ani.SetBool("RunChk", true);
        ani.SetFloat("Direction", x);


    }


    [Command]
    void CmdMove(float x, float z)
    {
        
    }

}
