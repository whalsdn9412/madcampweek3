﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoloCharacterScript : MonoBehaviour
{
    public GameObject Player;
    public Camera PlayerCamera;
    public Vector3 DifferPlayerAndCamera;
    public float speed;
    Animator ani;
    Rigidbody rbody;

    private Vector3 firstpoint; //change type on Vector3
    private Vector3 secondpoint;
    private float xAngle = 0.0f; //angle for axes x for rotation
    private float yAngle = 0.0f;
    private float xAngTemp = 0.0f; //temp variable for angle
    private float yAngTemp = 0.0f;
    private float angle = 0.0f;

    // Start is called before the first frame update
    private void Start()
    {
        rbody = Player.GetComponent<Rigidbody>();
        ani = GetComponent<Animator>();
    }


    private void Awake()
    {
        DifferPlayerAndCamera = Player.transform.position - PlayerCamera.transform.position;
    }

    // Update is called once per frame
    void Update()
    {


        //JoyStick.JoyVec;
        if (JoyStick.JoyVec.x == 0 && JoyStick.JoyVec.y == 0)
        {
            ani.SetBool("RunChk", false);
        }
        else
        {
            ani.SetBool("RunChk", true);
            var Theta = Mathf.Atan(JoyStick.JoyVec.y / JoyStick.JoyVec.x);
            Theta *= Mathf.Rad2Deg;
            Theta += 90.0f;
            Theta /= 90.0f;
            if (JoyStick.JoyVec.x < 0)
            {
                Theta += 2;
            }
            ani.SetFloat("Direction", Theta);
        }
        if (Input.touchCount == 1)
        {
            //Touch began, save position
            if (Input.GetTouch(0).phase == TouchPhase.Began)
            {
                firstpoint = Input.GetTouch(0).position;
                xAngTemp = xAngle;
                yAngTemp = yAngle;
            }
            //Move finger by screen
            if (Input.GetTouch(0).phase == TouchPhase.Moved)
            {
                if (Input.GetTouch(0).deltaPosition.x > 20 || Input.GetTouch(0).deltaPosition.x < -20)
                {

                    secondpoint = Input.GetTouch(0).position;
                    angle = (secondpoint[0] - firstpoint[0]) * 180.0f / Screen.width;
                    xAngle = xAngTemp + angle;
                    yAngle = yAngTemp - (secondpoint[1] - firstpoint[1]) * 90.0f / Screen.height;
                    Quaternion rotation = Quaternion.Euler(0, xAngle, 0);
                    Player.transform.rotation = rotation;
                }

            }
            Player.transform.position += (Player.transform.right * JoyStick.JoyVec.x) * speed;
            Player.transform.position += (Player.transform.forward * JoyStick.JoyVec.y) * speed;
        }
        else if (Input.touchCount == 2)
        {
            if (Input.GetTouch(1).phase == TouchPhase.Began)
            {
                firstpoint = Input.GetTouch(1).position;
                xAngTemp = xAngle;
                yAngTemp = yAngle;
            }
            //Move finger by screen
            if (Input.GetTouch(1).phase == TouchPhase.Moved)
            {
                secondpoint = Input.GetTouch(1).position;
                angle = (secondpoint[0] - firstpoint[0]) * 180.0f / Screen.width;
                xAngle = xAngTemp + angle;
                yAngle = yAngTemp - (secondpoint[1] - firstpoint[1]) * 90.0f / Screen.height;
                /*
                if (angle > 0)
                {
                    Player.transform.Rotate(new Vector3(0, Time.deltaTime * angle * 2, 0));
                }
                else if (angle < 0)
                {
                    Player.transform.Rotate(new Vector3(0, Time.deltaTime * angle * 2, 0));
                }*/

            }

            Quaternion rotation = Quaternion.Euler(0, xAngle, 0);
            if (Input.GetTouch(0).phase == TouchPhase.Stationary && Input.GetTouch(1).phase == TouchPhase.Moved)
            {
                Player.transform.rotation = rotation;
            }
            Player.transform.rotation = rotation;
            Player.transform.position += (Player.transform.right * JoyStick.JoyVec.x) * speed;
            Player.transform.position += (Player.transform.forward * JoyStick.JoyVec.y) * speed;
        }

        if (Input.GetKey(KeyCode.A))
        {
            Player.transform.Rotate(new Vector3(0, -Time.deltaTime * 40.0f, 0));
        }
        if (Input.GetKey(KeyCode.D))
        {
            Player.transform.Rotate(new Vector3(0, Time.deltaTime * 40.0f, 0));
        }
        if (Input.GetKey(KeyCode.UpArrow))
        {
            Player.transform.position += (Player.transform.forward) * speed;
        }
        if (Input.GetKey(KeyCode.DownArrow))
        {
            Player.transform.position -= (Player.transform.forward) * speed;
        }
        if (Input.GetKey(KeyCode.LeftArrow))
        {
            Player.transform.position -= (Player.transform.right) * speed;
        }
        if (Input.GetKey(KeyCode.RightArrow))
        {
            Player.transform.position += (Player.transform.right) * speed;
        }

        
    }

}

