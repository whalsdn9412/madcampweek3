﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Networking;

public class MultiMazeGenerator : NetworkBehaviour
{
    public GameObject Wall;
    public GameObject Ground;
    public GameObject Player;


    public struct MazeRoom
    {
        public int up, down, left, right;
        public MazeRoom(int up, int down, int left, int right)
        {
            this.up = up;
            this.down = down;
            this.left = left;
            this.right = right;
        }

        public bool CheckAllRoom()
        {

            if ((this.up == 0) || (this.down == 0) || (this.left == 0) || (this.right == 0))
            {
                return false;
            }
            return true;
        }

    }

    public struct Coordinate
    {
        public int x, y;
        public Coordinate(int x, int y)
        {
            this.x = x;
            this.y = y;
        }
    }

    Stack<Coordinate> coordinate = new Stack<Coordinate>();
    MazeRoom[,] mazeroom;

    readonly int UP = 1;
    readonly int DOWN = 2;
    readonly int LEFT = 4;
    readonly int RIGHT = 8;
    readonly int GARO = 10;
    readonly int SERO = 10;

    void MakeOneMaze()
    {
        mazeroom = new MazeRoom[GARO, SERO];
        // 방의 크기를 생성한다.
        for (int i = 0; i < GARO; i++)
        {
            for (int j = 0; j < SERO; j++)
            {
                mazeroom[i, j].up = 1;
                mazeroom[i, j].down = 1;
                mazeroom[i, j].left = 1;
                mazeroom[i, j].right = 1;
            }
        }
        // 시작점은 항상 0,0 으로 한다.
        coordinate.Push(new Coordinate(0, 0));
        while (coordinate.Count > 0)
        {
            ArrayList direction = new ArrayList();
            Coordinate c = coordinate.Pop();
            // 이 좌표 주변에 4개의 벽이 모두 부숴지지 않은 셀이 하나 이상 존재 한다면
            // 상 하 좌 우 순으로 체크한다.
            // 상은 c.y - 1 만 범위를 체크한다
            if (c.y > 0)
            {
                if (mazeroom[c.x, c.y - 1].CheckAllRoom())
                {
                    direction.Add(UP);
                }
            }
            // 하는 c.y + 1 만 범위를 체크한다
            if (c.y < SERO - 1)
            {
                if (mazeroom[c.x, c.y + 1].CheckAllRoom())
                {
                    direction.Add(DOWN);
                }
            }
            // 좌는 c.x - 1 만 범위를 체크한다
            if (c.x > 0)
            {
                if (mazeroom[c.x - 1, c.y].CheckAllRoom())
                {
                    direction.Add(LEFT);
                }
            }
            // 우는 c.x + 1 만 범위를 체크한다
            if (c.x < GARO - 1)
            {
                if (mazeroom[c.x + 1, c.y].CheckAllRoom())
                {
                    direction.Add(RIGHT);
                }
            }

            if (direction.Count == 0)
            {
                continue;
            }

            int Choice = Random.Range(0, direction.Count);
            Coordinate nextCoordinate = new Coordinate();
            if ((int)direction[Choice] == UP)
            {
                nextCoordinate.x = c.x;
                nextCoordinate.y = c.y - 1;
                mazeroom[c.x, c.y].up = 0;
                mazeroom[nextCoordinate.x, nextCoordinate.y].down = 0;

            }
            else if ((int)direction[Choice] == DOWN)
            {
                nextCoordinate.x = c.x;
                nextCoordinate.y = c.y + 1;
                mazeroom[c.x, c.y].down = 0;
                mazeroom[nextCoordinate.x, nextCoordinate.y].up = 0;
            }
            else if ((int)direction[Choice] == LEFT)
            {
                nextCoordinate.x = c.x - 1;
                nextCoordinate.y = c.y;
                mazeroom[c.x, c.y].left = 0;
                mazeroom[nextCoordinate.x, nextCoordinate.y].right = 0;
            }
            else if ((int)direction[Choice] == RIGHT)
            {
                nextCoordinate.x = c.x + 1;
                nextCoordinate.y = c.y;
                mazeroom[c.x, c.y].right = 0;
                mazeroom[nextCoordinate.x, nextCoordinate.y].left = 0;
            }

            coordinate.Push(c);
            coordinate.Push(nextCoordinate);
        }

        var mazeroomDebug = new int[GARO * 3, SERO * 3];
        for (int i = 0; i < GARO; i++)
        {
            for (int j = 0; j < SERO; j++)
            {
                mazeroomDebug[3 * i + 1, 3 * j + 1] = 1;
                if (mazeroom[i, j].up == 0)
                {
                    mazeroomDebug[3 * i + 1, 3 * j] = 1;
                }
                if (mazeroom[i, j].down == 0)
                {
                    mazeroomDebug[3 * i + 1, 3 * j + 2] = 1;
                }
                if (mazeroom[i, j].left == 0)
                {
                    mazeroomDebug[3 * i, 3 * j + 1] = 1;
                }
                if (mazeroom[i, j].right == 0)
                {
                    mazeroomDebug[3 * i + 2, 3 * j + 1] = 1;
                }
            }
        }
        var text = "";
        for (int i = 0; i < GARO * 3; i++)
        {

            for (int j = 0; j < SERO * 3; j++)
            {
                if (mazeroomDebug[i, j] == 1)
                {
                    text += "□";
                }
                if (mazeroomDebug[i, j] == 0)
                {
                    text += "■";
                }

            }
            text += "\n";
        }

        Debug.Log(text);
        //완성
        for (int i = 0; i < GARO; i++)
        {
            for (int j = 0; j < SERO; j++)
            {
                Vector3 groundPosition = new Vector3(i * 8, 0, j * 8);
                Instantiate(Ground, groundPosition, Quaternion.Euler(0, 0, 0));
                if (mazeroom[i, j].up == 1)
                {
                    Vector3 groundPosition2 = new Vector3(i * 8, 0, j * 8 - 4);
                    Instantiate(Wall, groundPosition2, Quaternion.Euler(0, 0, 0));
                }
                if (mazeroom[i, j].down == 1)
                {
                    Vector3 groundPosition2 = new Vector3(i * 8, 0, j * 8 + 4);
                    Instantiate(Wall, groundPosition2, Quaternion.Euler(0, 0, 0));
                }
                if (mazeroom[i, j].left == 1)
                {
                    Vector3 groundPosition2 = new Vector3(i * 8 - 4, 0, j * 8);
                    Instantiate(Wall, groundPosition2, Quaternion.Euler(0, 90, 0));
                }
                if (mazeroom[i, j].right == 1)
                {
                    Vector3 groundPosition2 = new Vector3(i * 8 + 4, 0, j * 8);
                    Instantiate(Wall, groundPosition2, Quaternion.Euler(0, 90, 0));
                }
            }
        }

        Player.transform.position = new Vector3(3f, 2f, 3f);




    }


    // Start is called before the first frame update
    void Start()
    {
//
             MakeOneMaze();
    }
    private void Awake()
    {
       
    }

    // Update is called once per frame
    void Update()
    {

    }
}
